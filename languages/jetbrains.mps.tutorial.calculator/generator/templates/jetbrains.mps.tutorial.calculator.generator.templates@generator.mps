<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:df29921e-e981-4830-b280-08ab52a24aea(jetbrains.mps.tutorial.calculator.generator.templates@generator)">
  <persistence version="9" />
  <languages>
    <use id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator" version="3" />
    <use id="d7706f63-9be2-479c-a3da-ae92af1e64d5" name="jetbrains.mps.lang.generator.generationContext" version="2" />
    <devkit ref="a2eb3a43-fcc2-4200-80dc-c60110c4862d(jetbrains.mps.devkit.templates)" />
  </languages>
  <imports>
    <import index="o9al" ref="r:80c8a99a-d201-4ada-9fb3-7626260b226b(jetbrains.mps.tutorial.calculator.structure)" />
    <import index="gsia" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:javax.swing.event(JDK/)" />
    <import index="z60i" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.awt(JDK/)" />
    <import index="r791" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:javax.swing.text(JDK/)" />
    <import index="dxuu" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:javax.swing(JDK/)" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" implicit="true" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="nn" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1182160077978" name="jetbrains.mps.baseLanguage.structure.AnonymousClassCreator" flags="nn" index="YeOm9">
        <child id="1182160096073" name="cls" index="YeSDq" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534760951" name="jetbrains.mps.baseLanguage.structure.ArrayType" flags="in" index="10Q1$e">
        <child id="1070534760952" name="componentType" index="10Q1$1" />
      </concept>
      <concept id="1068390468200" name="jetbrains.mps.baseLanguage.structure.FieldDeclaration" flags="ig" index="312cEg" />
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <child id="1165602531693" name="superclass" index="1zkMxy" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <property id="521412098689998745" name="nonStatic" index="2bfB8j" />
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
      <concept id="1170345865475" name="jetbrains.mps.baseLanguage.structure.AnonymousClass" flags="ig" index="1Y3b0j">
        <reference id="1170346070688" name="classifier" index="1Y3XeK" />
      </concept>
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="1095416546421" name="jetbrains.mps.lang.generator.structure.MappingConfiguration" flags="ig" index="bUwia">
        <child id="1200911492601" name="mappingLabel" index="2rTMjI" />
        <child id="1167514678247" name="rootMappingRule" index="3lj3bC" />
      </concept>
      <concept id="1168619357332" name="jetbrains.mps.lang.generator.structure.RootTemplateAnnotation" flags="lg" index="n94m4">
        <reference id="1168619429071" name="applicableConcept" index="n9lRv" />
      </concept>
      <concept id="1200911316486" name="jetbrains.mps.lang.generator.structure.MappingLabelDeclaration" flags="lg" index="2rT7sh">
        <reference id="1200911342686" name="sourceConcept" index="2rTdP9" />
        <reference id="1200913004646" name="targetConcept" index="2rZz_L" />
      </concept>
      <concept id="1167169188348" name="jetbrains.mps.lang.generator.structure.TemplateFunctionParameter_sourceNode" flags="nn" index="30H73N" />
      <concept id="1167169308231" name="jetbrains.mps.lang.generator.structure.BaseMappingRule" flags="ng" index="30H$t8">
        <reference id="1167169349424" name="applicableConcept" index="30HIoZ" />
      </concept>
      <concept id="1087833241328" name="jetbrains.mps.lang.generator.structure.PropertyMacro" flags="ln" index="17Uvod">
        <child id="1167756362303" name="propertyValueFunction" index="3zH0cK" />
      </concept>
      <concept id="1087833466690" name="jetbrains.mps.lang.generator.structure.NodeMacro" flags="lg" index="17VmuZ">
        <reference id="1200912223215" name="mappingLabel" index="2rW$FS" />
      </concept>
      <concept id="1167514355419" name="jetbrains.mps.lang.generator.structure.Root_MappingRule" flags="lg" index="3lhOvk">
        <reference id="1167514355421" name="template" index="3lhOvi" />
      </concept>
      <concept id="1167756080639" name="jetbrains.mps.lang.generator.structure.PropertyMacro_GetPropertyValue" flags="in" index="3zFVjK" />
      <concept id="1167770111131" name="jetbrains.mps.lang.generator.structure.ReferenceMacro_GetReferent" flags="in" index="3$xsQk" />
      <concept id="1167951910403" name="jetbrains.mps.lang.generator.structure.SourceSubstituteMacro_SourceNodesQuery" flags="in" index="3JmXsc" />
      <concept id="1118786554307" name="jetbrains.mps.lang.generator.structure.LoopMacro" flags="ln" index="1WS0z7">
        <child id="1167952069335" name="sourceNodesQuery" index="3Jn$fo" />
      </concept>
      <concept id="1088761943574" name="jetbrains.mps.lang.generator.structure.ReferenceMacro" flags="ln" index="1ZhdrF">
        <child id="1167770376702" name="referentFunction" index="3$ytzL" />
      </concept>
    </language>
    <language id="d7706f63-9be2-479c-a3da-ae92af1e64d5" name="jetbrains.mps.lang.generator.generationContext">
      <concept id="1218047638031" name="jetbrains.mps.lang.generator.generationContext.structure.GenerationContextOp_CreateUniqueName" flags="nn" index="2piZGk">
        <child id="1218047638032" name="baseName" index="2piZGb" />
      </concept>
      <concept id="1216860049627" name="jetbrains.mps.lang.generator.generationContext.structure.GenerationContextOp_GetOutputByLabelAndInput" flags="nn" index="1iwH70">
        <reference id="1216860049628" name="label" index="1iwH77" />
        <child id="1216860049632" name="inputNode" index="1iwH7V" />
      </concept>
      <concept id="1216860049635" name="jetbrains.mps.lang.generator.generationContext.structure.TemplateFunctionParameter_generationContext" flags="nn" index="1iwH7S" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="3364660638048049750" name="jetbrains.mps.lang.core.structure.PropertyAttribute" flags="ng" index="A9Btg">
        <property id="1757699476691236117" name="name_DebugInfo" index="2qtEX9" />
        <property id="1341860900487648621" name="propertyId" index="P4ACc" />
      </concept>
      <concept id="3364660638048049745" name="jetbrains.mps.lang.core.structure.LinkAttribute" flags="ng" index="A9Btn">
        <property id="1757699476691236116" name="role_DebugInfo" index="2qtEX8" />
        <property id="1341860900488019036" name="linkId" index="P3scX" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="bUwia" id="6CZCWpa4uaB">
    <property role="TrG5h" value="main" />
    <node concept="3lhOvk" id="5g7NFXwmw8R" role="3lj3bC">
      <ref role="30HIoZ" to="o9al:5g7NFXwlPTm" resolve="Calculator" />
      <ref role="3lhOvi" node="5g7NFXwmw7B" resolve="CalculatorImpl" />
    </node>
    <node concept="2rT7sh" id="5g7NFXwoO0u" role="2rTMjI">
      <property role="TrG5h" value="InputFieldDeclaration" />
      <ref role="2rTdP9" to="o9al:5g7NFXwmalv" resolve="InputField" />
      <ref role="2rZz_L" to="tpee:fz12cDC" resolve="FieldDeclaration" />
    </node>
  </node>
  <node concept="312cEu" id="5g7NFXwmw7B">
    <property role="TrG5h" value="CalculatorImpl" />
    <node concept="312cEg" id="5g7NFXwnt4B" role="jymVt">
      <property role="TrG5h" value="listener" />
      <node concept="3Tm6S6" id="5g7NFXwnsRZ" role="1B3o_S" />
      <node concept="3uibUv" id="5g7NFXwnt4g" role="1tU5fm">
        <ref role="3uigEE" to="gsia:~DocumentListener" resolve="DocumentListener" />
      </node>
      <node concept="2ShNRf" id="5g7NFXwntic" role="33vP2m">
        <node concept="YeOm9" id="5g7NFXwnwnK" role="2ShVmc">
          <node concept="1Y3b0j" id="5g7NFXwnwnN" role="YeSDq">
            <property role="2bfB8j" value="true" />
            <ref role="1Y3XeK" to="gsia:~DocumentListener" resolve="DocumentListener" />
            <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
            <node concept="3Tm1VV" id="5g7NFXwnwnO" role="1B3o_S" />
            <node concept="3clFb_" id="5g7NFXwnwnT" role="jymVt">
              <property role="TrG5h" value="insertUpdate" />
              <node concept="3Tm1VV" id="5g7NFXwnwnU" role="1B3o_S" />
              <node concept="3cqZAl" id="5g7NFXwnwnW" role="3clF45" />
              <node concept="37vLTG" id="5g7NFXwnwnX" role="3clF46">
                <property role="TrG5h" value="p1" />
                <node concept="3uibUv" id="5g7NFXwnwnY" role="1tU5fm">
                  <ref role="3uigEE" to="gsia:~DocumentEvent" resolve="DocumentEvent" />
                </node>
              </node>
              <node concept="3clFbS" id="5g7NFXwnwnZ" role="3clF47">
                <node concept="3clFbF" id="5g7NFXwnwEI" role="3cqZAp">
                  <node concept="1rXfSq" id="5g7NFXwnwEH" role="3clFbG">
                    <ref role="37wK5l" node="5g7NFXwnADc" resolve="update" />
                  </node>
                </node>
              </node>
              <node concept="2AHcQZ" id="5g7NFXwnwo1" role="2AJF6D">
                <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
              </node>
            </node>
            <node concept="2tJIrI" id="5g7NFXwnwo2" role="jymVt" />
            <node concept="3clFb_" id="5g7NFXwnwo3" role="jymVt">
              <property role="TrG5h" value="removeUpdate" />
              <node concept="3Tm1VV" id="5g7NFXwnwo4" role="1B3o_S" />
              <node concept="3cqZAl" id="5g7NFXwnwo6" role="3clF45" />
              <node concept="37vLTG" id="5g7NFXwnwo7" role="3clF46">
                <property role="TrG5h" value="p1" />
                <node concept="3uibUv" id="5g7NFXwnwo8" role="1tU5fm">
                  <ref role="3uigEE" to="gsia:~DocumentEvent" resolve="DocumentEvent" />
                </node>
              </node>
              <node concept="3clFbS" id="5g7NFXwnwo9" role="3clF47">
                <node concept="3clFbF" id="5g7NFXwnxil" role="3cqZAp">
                  <node concept="1rXfSq" id="5g7NFXwnxik" role="3clFbG">
                    <ref role="37wK5l" node="5g7NFXwnADc" resolve="update" />
                  </node>
                </node>
              </node>
              <node concept="2AHcQZ" id="5g7NFXwnwob" role="2AJF6D">
                <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
              </node>
            </node>
            <node concept="2tJIrI" id="5g7NFXwnwoc" role="jymVt" />
            <node concept="3clFb_" id="5g7NFXwnwod" role="jymVt">
              <property role="TrG5h" value="changedUpdate" />
              <node concept="3Tm1VV" id="5g7NFXwnwoe" role="1B3o_S" />
              <node concept="3cqZAl" id="5g7NFXwnwog" role="3clF45" />
              <node concept="37vLTG" id="5g7NFXwnwoh" role="3clF46">
                <property role="TrG5h" value="p1" />
                <node concept="3uibUv" id="5g7NFXwnwoi" role="1tU5fm">
                  <ref role="3uigEE" to="gsia:~DocumentEvent" resolve="DocumentEvent" />
                </node>
              </node>
              <node concept="3clFbS" id="5g7NFXwnwoj" role="3clF47">
                <node concept="3clFbF" id="5g7NFXwnxuD" role="3cqZAp">
                  <node concept="1rXfSq" id="5g7NFXwnxuC" role="3clFbG">
                    <ref role="37wK5l" node="5g7NFXwnADc" resolve="update" />
                  </node>
                </node>
              </node>
              <node concept="2AHcQZ" id="5g7NFXwnwol" role="2AJF6D">
                <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="5g7NFXwoaMU" role="jymVt" />
    <node concept="312cEg" id="5g7NFXwodeN" role="jymVt">
      <property role="TrG5h" value="inputField" />
      <node concept="3Tm6S6" id="5g7NFXwobu4" role="1B3o_S" />
      <node concept="3uibUv" id="5g7NFXwodbE" role="1tU5fm">
        <ref role="3uigEE" to="dxuu:~JTextField" resolve="JTextField" />
      </node>
      <node concept="2ShNRf" id="5g7NFXwodOA" role="33vP2m">
        <node concept="1pGfFk" id="5g7NFXwog2U" role="2ShVmc">
          <ref role="37wK5l" to="dxuu:~JTextField.&lt;init&gt;()" resolve="JTextField" />
        </node>
      </node>
      <node concept="1WS0z7" id="5g7NFXwoOJ4" role="lGtFl">
        <ref role="2rW$FS" node="5g7NFXwoO0u" resolve="InputFieldDeclaration" />
        <node concept="3JmXsc" id="5g7NFXwoOJ5" role="3Jn$fo">
          <node concept="3clFbS" id="5g7NFXwoOJ6" role="2VODD2">
            <node concept="3clFbF" id="5g7NFXwoP$u" role="3cqZAp">
              <node concept="2OqwBi" id="5g7NFXwoPKH" role="3clFbG">
                <node concept="30H73N" id="5g7NFXwoP$t" role="2Oq$k0" />
                <node concept="3Tsc0h" id="5g7NFXwoPTE" role="2OqNvi">
                  <ref role="3TtcxE" to="o9al:5g7NFXwmamf" resolve="inputField" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17Uvod" id="5g7NFXwohpH" role="lGtFl">
        <property role="2qtEX9" value="name" />
        <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
        <node concept="3zFVjK" id="5g7NFXwohpI" role="3zH0cK">
          <node concept="3clFbS" id="5g7NFXwohpJ" role="2VODD2">
            <node concept="3clFbF" id="5g7NFXwoiag" role="3cqZAp">
              <node concept="2OqwBi" id="5g7NFXwoiQ3" role="3clFbG">
                <node concept="1iwH7S" id="5g7NFXwoiaf" role="2Oq$k0" />
                <node concept="2piZGk" id="5g7NFXwoiVg" role="2OqNvi">
                  <node concept="Xl_RD" id="5g7NFXwojv3" role="2piZGb">
                    <property role="Xl_RC" value="inputField" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="312cEg" id="5g7NFXwomv2" role="jymVt">
      <property role="TrG5h" value="outputField" />
      <node concept="3Tm6S6" id="5g7NFXwokDI" role="1B3o_S" />
      <node concept="3uibUv" id="5g7NFXwomrS" role="1tU5fm">
        <ref role="3uigEE" to="dxuu:~JTextField" resolve="JTextField" />
      </node>
      <node concept="2ShNRf" id="5g7NFXwonsl" role="33vP2m">
        <node concept="1pGfFk" id="5g7NFXwopok" role="2ShVmc">
          <ref role="37wK5l" to="dxuu:~JTextField.&lt;init&gt;()" resolve="JTextField" />
        </node>
      </node>
      <node concept="1WS0z7" id="5g7NFXwopug" role="lGtFl">
        <ref role="2rW$FS" node="5g7NFXwoO0u" resolve="InputFieldDeclaration" />
        <node concept="3JmXsc" id="5g7NFXwopuh" role="3Jn$fo">
          <node concept="3clFbS" id="5g7NFXwopui" role="2VODD2">
            <node concept="3clFbF" id="5g7NFXword_" role="3cqZAp">
              <node concept="2OqwBi" id="5g7NFXworpO" role="3clFbG">
                <node concept="30H73N" id="5g7NFXword$" role="2Oq$k0" />
                <node concept="3Tsc0h" id="5g7NFXwor$G" role="2OqNvi">
                  <ref role="3TtcxE" to="o9al:5g7NFXwmdDR" resolve="outputField" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17Uvod" id="5g7NFXworSn" role="lGtFl">
        <property role="2qtEX9" value="name" />
        <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
        <node concept="3zFVjK" id="5g7NFXworSo" role="3zH0cK">
          <node concept="3clFbS" id="5g7NFXworSp" role="2VODD2">
            <node concept="3clFbF" id="5g7NFXwot7u" role="3cqZAp">
              <node concept="2OqwBi" id="5g7NFXwotOD" role="3clFbG">
                <node concept="1iwH7S" id="5g7NFXwot7t" role="2Oq$k0" />
                <node concept="2piZGk" id="5g7NFXwotWw" role="2OqNvi">
                  <node concept="Xl_RD" id="5g7NFXwou$w" role="2piZGb">
                    <property role="Xl_RC" value="outputField" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="5g7NFXwok3A" role="jymVt" />
    <node concept="3clFbW" id="5g7NFXwnNen" role="jymVt">
      <node concept="3cqZAl" id="5g7NFXwnNeo" role="3clF45" />
      <node concept="3clFbS" id="5g7NFXwnNeq" role="3clF47">
        <node concept="3clFbF" id="5g7NFXwnOzZ" role="3cqZAp">
          <node concept="1rXfSq" id="5g7NFXwnOzY" role="3clFbG">
            <ref role="37wK5l" to="z60i:~Frame.setTitle(java.lang.String)" resolve="setTitle" />
            <node concept="Xl_RD" id="5g7NFXwo9rT" role="37wK5m">
              <property role="Xl_RC" value="CalculatorImpl" />
              <node concept="17Uvod" id="5g7NFXwo9yz" role="lGtFl">
                <property role="2qtEX9" value="value" />
                <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1070475926800/1070475926801" />
                <node concept="3zFVjK" id="5g7NFXwo9y$" role="3zH0cK">
                  <node concept="3clFbS" id="5g7NFXwo9y_" role="2VODD2">
                    <node concept="3clFbF" id="5g7NFXwoae2" role="3cqZAp">
                      <node concept="2OqwBi" id="5g7NFXwoaqk" role="3clFbG">
                        <node concept="30H73N" id="5g7NFXwoae1" role="2Oq$k0" />
                        <node concept="3TrcHB" id="5g7NFXwoazh" role="2OqNvi">
                          <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5g7NFXwnQiu" role="3cqZAp">
          <node concept="1rXfSq" id="5g7NFXwnQis" role="3clFbG">
            <ref role="37wK5l" to="dxuu:~JFrame.setLayout(java.awt.LayoutManager)" resolve="setLayout" />
            <node concept="2ShNRf" id="5g7NFXwnVjz" role="37wK5m">
              <node concept="1pGfFk" id="5g7NFXwnXd7" role="2ShVmc">
                <ref role="37wK5l" to="z60i:~GridLayout.&lt;init&gt;(int,int)" resolve="GridLayout" />
                <node concept="3cmrfG" id="5g7NFXwnXMm" role="37wK5m">
                  <property role="3cmrfH" value="0" />
                </node>
                <node concept="3cmrfG" id="5g7NFXwnZ3d" role="37wK5m">
                  <property role="3cmrfH" value="2" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5g7NFXwoz1T" role="3cqZAp" />
        <node concept="9aQIb" id="5g7NFXwo$on" role="3cqZAp">
          <node concept="3clFbS" id="5g7NFXwo$op" role="9aQI4">
            <node concept="3clFbF" id="5g7NFXwo_yo" role="3cqZAp">
              <node concept="2OqwBi" id="5g7NFXwoC1b" role="3clFbG">
                <node concept="2OqwBi" id="5g7NFXwoALv" role="2Oq$k0">
                  <node concept="37vLTw" id="5g7NFXwo_ym" role="2Oq$k0">
                    <ref role="3cqZAo" node="5g7NFXwodeN" resolve="inputField" />
                    <node concept="1ZhdrF" id="5g7NFXwoQYd" role="lGtFl">
                      <property role="2qtEX8" value="variableDeclaration" />
                      <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
                      <node concept="3$xsQk" id="5g7NFXwoQYe" role="3$ytzL">
                        <node concept="3clFbS" id="5g7NFXwoQYf" role="2VODD2">
                          <node concept="3clFbF" id="5g7NFXwoSdD" role="3cqZAp">
                            <node concept="2OqwBi" id="5g7NFXwoSV4" role="3clFbG">
                              <node concept="1iwH7S" id="5g7NFXwoSdC" role="2Oq$k0" />
                              <node concept="1iwH70" id="5g7NFXwoT0L" role="2OqNvi">
                                <ref role="1iwH77" node="5g7NFXwoO0u" resolve="InputFieldDeclaration" />
                                <node concept="30H73N" id="5g7NFXwoUgu" role="1iwH7V" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="5g7NFXwoBKI" role="2OqNvi">
                    <ref role="37wK5l" to="r791:~JTextComponent.getDocument()" resolve="getDocument" />
                  </node>
                </node>
                <node concept="liA8E" id="5g7NFXwoCoG" role="2OqNvi">
                  <ref role="37wK5l" to="r791:~Document.addDocumentListener(javax.swing.event.DocumentListener)" resolve="addDocumentListener" />
                  <node concept="37vLTw" id="5g7NFXwoD_s" role="37wK5m">
                    <ref role="3cqZAo" node="5g7NFXwnt4B" resolve="listener" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="5g7NFXwoEvB" role="3cqZAp">
              <node concept="1rXfSq" id="5g7NFXwoEv_" role="3clFbG">
                <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component)" resolve="add" />
                <node concept="2ShNRf" id="5g7NFXwoFh9" role="37wK5m">
                  <node concept="1pGfFk" id="5g7NFXwoHb3" role="2ShVmc">
                    <ref role="37wK5l" to="dxuu:~JLabel.&lt;init&gt;(java.lang.String)" resolve="JLabel" />
                    <node concept="Xl_RD" id="5g7NFXwoHOC" role="37wK5m">
                      <property role="Xl_RC" value="Title" />
                      <node concept="17Uvod" id="5g7NFXwoMym" role="lGtFl">
                        <property role="2qtEX9" value="value" />
                        <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1070475926800/1070475926801" />
                        <node concept="3zFVjK" id="5g7NFXwoMyn" role="3zH0cK">
                          <node concept="3clFbS" id="5g7NFXwoMyo" role="2VODD2">
                            <node concept="3clFbF" id="5g7NFXwoNoi" role="3cqZAp">
                              <node concept="2OqwBi" id="5g7NFXwoN_9" role="3clFbG">
                                <node concept="30H73N" id="5g7NFXwoNoh" role="2Oq$k0" />
                                <node concept="3TrcHB" id="5g7NFXwoNO1" role="2OqNvi">
                                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="5g7NFXwoIMv" role="3cqZAp">
              <node concept="1rXfSq" id="5g7NFXwoIMt" role="3clFbG">
                <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component)" resolve="add" />
                <node concept="37vLTw" id="5g7NFXwoK2O" role="37wK5m">
                  <ref role="3cqZAo" node="5g7NFXwodeN" resolve="inputField" />
                  <node concept="1ZhdrF" id="5g7NFXwoV5c" role="lGtFl">
                    <property role="2qtEX8" value="variableDeclaration" />
                    <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
                    <node concept="3$xsQk" id="5g7NFXwoV5d" role="3$ytzL">
                      <node concept="3clFbS" id="5g7NFXwoV5e" role="2VODD2">
                        <node concept="3clFbF" id="5g7NFXwoWrm" role="3cqZAp">
                          <node concept="2OqwBi" id="5g7NFXwoXbx" role="3clFbG">
                            <node concept="1iwH7S" id="5g7NFXwoWrl" role="2Oq$k0" />
                            <node concept="1iwH70" id="5g7NFXwoXhe" role="2OqNvi">
                              <ref role="1iwH77" node="5g7NFXwoO0u" resolve="InputFieldDeclaration" />
                              <node concept="30H73N" id="5g7NFXwoYwV" role="1iwH7V" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1WS0z7" id="5g7NFXwoL93" role="lGtFl">
            <node concept="3JmXsc" id="5g7NFXwoL94" role="3Jn$fo">
              <node concept="3clFbS" id="5g7NFXwoL95" role="2VODD2">
                <node concept="3clFbF" id="5g7NFXwoLSK" role="3cqZAp">
                  <node concept="2OqwBi" id="5g7NFXwoM4Z" role="3clFbG">
                    <node concept="30H73N" id="5g7NFXwoLSJ" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="5g7NFXwoMe4" role="2OqNvi">
                      <ref role="3TtcxE" to="o9al:5g7NFXwmamf" resolve="inputField" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5g7NFXwoz2l" role="3cqZAp" />
        <node concept="3clFbF" id="5g7NFXwo0aw" role="3cqZAp">
          <node concept="1rXfSq" id="5g7NFXwo0au" role="3clFbG">
            <ref role="37wK5l" node="5g7NFXwnADc" resolve="update" />
          </node>
        </node>
        <node concept="3clFbF" id="5g7NFXwo1pq" role="3cqZAp">
          <node concept="1rXfSq" id="5g7NFXwo1po" role="3clFbG">
            <ref role="37wK5l" to="dxuu:~JFrame.setDefaultCloseOperation(int)" resolve="setDefaultCloseOperation" />
            <node concept="10M0yZ" id="5g7NFXwo2Db" role="37wK5m">
              <ref role="3cqZAo" to="dxuu:~WindowConstants.EXIT_ON_CLOSE" resolve="EXIT_ON_CLOSE" />
              <ref role="1PxDUh" to="dxuu:~JFrame" resolve="JFrame" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5g7NFXwo3Rc" role="3cqZAp">
          <node concept="1rXfSq" id="5g7NFXwo3Ra" role="3clFbG">
            <ref role="37wK5l" to="z60i:~Window.pack()" resolve="pack" />
          </node>
        </node>
        <node concept="3clFbF" id="5g7NFXwo53S" role="3cqZAp">
          <node concept="1rXfSq" id="5g7NFXwo53Q" role="3clFbG">
            <ref role="37wK5l" to="z60i:~Window.setVisible(boolean)" resolve="setVisible" />
            <node concept="3clFbT" id="5g7NFXwo5HY" role="37wK5m">
              <property role="3clFbU" value="true" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="5g7NFXwnNer" role="1B3o_S" />
    </node>
    <node concept="2tJIrI" id="5g7NFXwn_Vg" role="jymVt" />
    <node concept="3clFb_" id="5g7NFXwnADc" role="jymVt">
      <property role="TrG5h" value="update" />
      <node concept="3clFbS" id="5g7NFXwnADf" role="3clF47" />
      <node concept="3Tm1VV" id="5g7NFXwnAli" role="1B3o_S" />
      <node concept="3cqZAl" id="5g7NFXwnABT" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="5g7NFXwnANo" role="jymVt" />
    <node concept="2YIFZL" id="5g7NFXwnBI2" role="jymVt">
      <property role="TrG5h" value="main" />
      <node concept="3clFbS" id="5g7NFXwnBI5" role="3clF47">
        <node concept="3clFbF" id="5g7NFXwnDhN" role="3cqZAp">
          <node concept="2YIFZM" id="5g7NFXwnDib" role="3clFbG">
            <ref role="37wK5l" to="dxuu:~SwingUtilities.invokeLater(java.lang.Runnable)" resolve="invokeLater" />
            <ref role="1Pybhc" to="dxuu:~SwingUtilities" resolve="SwingUtilities" />
            <node concept="2ShNRf" id="5g7NFXwnEoS" role="37wK5m">
              <node concept="YeOm9" id="5g7NFXwnGQK" role="2ShVmc">
                <node concept="1Y3b0j" id="5g7NFXwnGQN" role="YeSDq">
                  <property role="2bfB8j" value="true" />
                  <ref role="1Y3XeK" to="wyt6:~Runnable" resolve="Runnable" />
                  <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                  <node concept="3Tm1VV" id="5g7NFXwnGQO" role="1B3o_S" />
                  <node concept="3clFb_" id="5g7NFXwnGQT" role="jymVt">
                    <property role="TrG5h" value="run" />
                    <node concept="3Tm1VV" id="5g7NFXwnGQU" role="1B3o_S" />
                    <node concept="3cqZAl" id="5g7NFXwnGQW" role="3clF45" />
                    <node concept="3clFbS" id="5g7NFXwnGQX" role="3clF47">
                      <node concept="3clFbF" id="5g7NFXwnHDW" role="3cqZAp">
                        <node concept="2ShNRf" id="5g7NFXwnHDU" role="3clFbG">
                          <node concept="1pGfFk" id="5g7NFXwnNpB" role="2ShVmc">
                            <ref role="37wK5l" node="5g7NFXwnNen" resolve="CalculatorImpl" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2AHcQZ" id="5g7NFXwnGQZ" role="2AJF6D">
                      <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="5g7NFXwnBdF" role="1B3o_S" />
      <node concept="3cqZAl" id="5g7NFXwnBHs" role="3clF45" />
      <node concept="37vLTG" id="5g7NFXwnC11" role="3clF46">
        <property role="TrG5h" value="args" />
        <node concept="10Q1$e" id="5g7NFXwnC3x" role="1tU5fm">
          <node concept="17QB3L" id="5g7NFXwnC10" role="10Q1$1" />
        </node>
      </node>
    </node>
    <node concept="3Tm1VV" id="5g7NFXwmw7C" role="1B3o_S" />
    <node concept="n94m4" id="5g7NFXwmw7D" role="lGtFl">
      <ref role="n9lRv" to="o9al:5g7NFXwlPTm" resolve="Calculator" />
    </node>
    <node concept="17Uvod" id="5g7NFXwmVSz" role="lGtFl">
      <property role="2qtEX9" value="name" />
      <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
      <node concept="3zFVjK" id="5g7NFXwmVS$" role="3zH0cK">
        <node concept="3clFbS" id="5g7NFXwmVS_" role="2VODD2">
          <node concept="3clFbF" id="5g7NFXwmWtT" role="3cqZAp">
            <node concept="2OqwBi" id="5g7NFXwmWzQ" role="3clFbG">
              <node concept="30H73N" id="5g7NFXwmWza" role="2Oq$k0" />
              <node concept="3TrcHB" id="5g7NFXwmW_4" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3uibUv" id="5g7NFXwn_zi" role="1zkMxy">
      <ref role="3uigEE" to="dxuu:~JFrame" resolve="JFrame" />
    </node>
  </node>
</model>

