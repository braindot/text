<?xml version="1.0" encoding="UTF-8"?>
<solution name="jetbrains.mps.tutorial.calculator.sandbox" uuid="3143e127-0fe2-4329-a839-22c70928af49" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <facets>
    <facet type="java">
      <classes generated="true" path="${module}/classes_gen" />
    </facet>
  </facets>
  <sourcePath />
  <languageVersions>
    <language slang="l:f3061a53-9226-4cc5-a443-f952ceaf5816:jetbrains.mps.baseLanguage" version="11" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="2" />
    <language slang="l:9ded098b-ad6a-4657-bfd9-48636cfe8bc3:jetbrains.mps.lang.traceable" version="0" />
    <language slang="l:edd5305b-bd11-4890-b778-0aa67805d67b:jetbrains.mps.tutorial.calculator" version="0" />
  </languageVersions>
  <dependencyVersions>
    <module reference="3143e127-0fe2-4329-a839-22c70928af49(jetbrains.mps.tutorial.calculator.sandbox)" version="0" />
  </dependencyVersions>
</solution>

