<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:de35d881-cd4d-43b7-a5ab-c3d1bcab8d93(jetbrains.mps.tutorial.calculator.sandbox)">
  <persistence version="9" />
  <languages>
    <use id="edd5305b-bd11-4890-b778-0aa67805d67b" name="jetbrains.mps.tutorial.calculator" version="0" />
  </languages>
  <imports />
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
    </language>
    <language id="edd5305b-bd11-4890-b778-0aa67805d67b" name="jetbrains.mps.tutorial.calculator">
      <concept id="6055035545373335126" name="jetbrains.mps.tutorial.calculator.structure.Calculator" flags="ng" index="Ok93D">
        <child id="6055035545373432439" name="outputField" index="OnLj8" />
        <child id="6055035545373418895" name="inputField" index="OnQGK" />
      </concept>
      <concept id="6055035545373447959" name="jetbrains.mps.tutorial.calculator.structure.InputFieldReference" flags="ng" index="OnHAC">
        <reference id="6055035545373447960" name="inputField" index="OnHAB" />
      </concept>
      <concept id="6055035545373432408" name="jetbrains.mps.tutorial.calculator.structure.OutputField" flags="ng" index="OnLjB">
        <child id="6055035545373432802" name="expression" index="OnLlt" />
      </concept>
      <concept id="6055035545373418847" name="jetbrains.mps.tutorial.calculator.structure.InputField" flags="ng" index="OnQJw" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="Ok93D" id="5g7NFXwmalu">
    <property role="TrG5h" value="MyCalc" />
    <node concept="OnQJw" id="5g7NFXwmdDa" role="OnQGK">
      <property role="TrG5h" value="width" />
    </node>
    <node concept="OnQJw" id="5g7NFXwmdDc" role="OnQGK">
      <property role="TrG5h" value="height" />
    </node>
    <node concept="OnQJw" id="5g7NFXwmdDf" role="OnQGK">
      <property role="TrG5h" value="depth" />
    </node>
    <node concept="OnLjB" id="5g7NFXwmfgs" role="OnLj8">
      <node concept="3cpWs3" id="5g7NFXwmw6A" role="OnLlt">
        <node concept="OnHAC" id="5g7NFXwmw7d" role="3uHU7w">
          <ref role="OnHAB" node="5g7NFXwmdDa" resolve="width" />
        </node>
        <node concept="3cpWs3" id="5g7NFXwmhpR" role="3uHU7B">
          <node concept="3cpWs3" id="5g7NFXwmhbu" role="3uHU7B">
            <node concept="3cmrfG" id="5g7NFXwmgLa" role="3uHU7B">
              <property role="3cmrfH" value="2" />
            </node>
            <node concept="3cmrfG" id="5g7NFXwmhbE" role="3uHU7w">
              <property role="3cmrfH" value="3" />
            </node>
          </node>
          <node concept="3cmrfG" id="5g7NFXwmhrK" role="3uHU7w">
            <property role="3cmrfH" value="5" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

